is_adventurer = {
	OR = {
	
		#Greentide
		tag = B02
		tag = B03
		tag = B04
		tag = B05
		tag = B06
		tag = B07
		tag = B08
		tag = B09
		tag = B10
		tag = B11
		tag = B12
		tag = B13
		tag = B14
		tag = B15
		tag = B16
		tag = B17
		tag = B18
		tag = B19
		tag = B20
		
		#Aelantir
		tag = H42
		tag = H40
		tag = H50
		tag = H48
		tag = H46
		tag = H52
		tag = H44
		tag = H56
		tag = H54
		tag = H60
		tag = H58
		tag = H64
		tag = H62
		tag = H66
		
		#Dwarovar
		tag = H69
		tag = H70
		tag = H71
		tag = H96
		tag = H97
		tag = H98
		tag = H68
		tag = H95

	}
}

is_temple = {
	OR = {
		#Cannor
		tag = A10 #Minaran Temple
		tag = A41 #Damish Temple
		tag = A43 #Ryalan Temple
		tag = A53 #Aramari Temple
	}
}

has_no_ward_province_modifiers = {
	NOT = { has_province_modifier = magic_realm_abjuration_empowered_ward}
	NOT = { has_province_modifier = magic_realm_abjuration_ward}
	NOT = { has_province_modifier = magic_estate_ward }
}

enhance_ability_cost_adm_trigger = {
	if = {
		limit = { adm = 0 }
		adm_power = 100
	}
	else_if = {
		limit = { adm = 1 }
		adm_power = 100
	}
	else_if = {
		limit = { adm = 2 }
		adm_power = 200
	}
	else_if = {
		limit = { adm = 3 }
		adm_power = 300
	}
	else_if = {
		limit = { adm = 4 }
		adm_power = 400
	}
	else_if = {
		limit = { adm = 5 }
		adm_power = 500
	}
}

enhance_ability_cost_dip_trigger = {
	if = {
		limit = { dip = 0 }
		dip_power = 100
	}
	else_if = {
		limit = { dip = 1 }
		dip_power = 100
	}
	else_if = {
		limit = { dip = 2 }
		dip_power = 200
	}
	else_if = {
		limit = { dip = 3 }
		dip_power = 300
	}
	else_if = {
		limit = { dip = 4 }
		dip_power = 400
	}
	else_if = {
		limit = { dip = 5 }
		dip_power = 500
	}
}

enhance_ability_cost_mil_trigger = {
	if = {
		limit = { mil = 0 }
		mil_power = 100
	}
	else_if = {
		limit = { mil = 1 }
		mil_power = 100
	}
	else_if = {
		limit = { mil = 2 }
		mil_power = 200
	}
	else_if = {
		limit = { mil = 3 }
		mil_power = 300
	}
	else_if = {
		limit = { mil = 4 }
		mil_power = 400
	}
	else_if = {
		limit = { mil = 5 }
		mil_power = 500
	}
}



province_with_farm_goods = {
	OR = {
		trade_goods = grain
		trade_goods = livestock
		trade_goods = wine
		trade_goods = spices
		trade_goods = tea
		trade_goods = coffee
		trade_goods = cocoa
		trade_goods = cotton
		trade_goods = sugar
		trade_goods = tobacco
		trade_goods = silk
	}
}


province_with_mineable_goods = {
	OR = {
		trade_goods = gold
		trade_goods = copper
		trade_goods = iron
		trade_goods = mithril
		trade_goods = gems
		trade_goods = salt
	}
}

province_with_urban_goods = {
	OR = {
		trade_goods = glass
		trade_goods = paper
		trade_goods = cloth
		trade_goods = chinaware
	}
}

is_abjuration_1 = {
		has_ruler_flag = abjuration_1
		NOT = { has_ruler_flag = abjuration_2 }
		NOT = { has_ruler_flag = abjuration_3 }
}

is_abjuration_2 = {
		has_ruler_flag = abjuration_2
		NOT = { has_ruler_flag = abjuration_3 }
}

is_abjuration_3 = {
		has_ruler_flag = abjuration_3
}

is_conjuration_1 = {
		has_ruler_flag = conjuration_1
		NOT = { has_ruler_flag = conjuration_2 }
		NOT = { has_ruler_flag = conjuration_3 }
}

is_conjuration_2 = {
		has_ruler_flag = conjuration_2
		NOT = { has_ruler_flag = conjuration_3 }
}

is_conjuration_3 = {
		has_ruler_flag = conjuration_3
}

is_divination_1 = {
		has_ruler_flag = divination_1
		NOT = { has_ruler_flag = divination_2 }
		NOT = { has_ruler_flag = divination_3 }
}

is_divination_2 = {
		has_ruler_flag = divination_2
		NOT = { has_ruler_flag = divination_3 }
}

is_divination_3 = {
		has_ruler_flag = divination_3
}

is_enchantment_1 = {
		has_ruler_flag = enchantment_1
		NOT = { has_ruler_flag = enchantment_2 }
		NOT = { has_ruler_flag = enchantment_3 }
}

is_enchantment_2 = {
		has_ruler_flag = enchantment_2
		NOT = { has_ruler_flag = enchantment_3 }
}

is_enchantment_3 = {
		has_ruler_flag = enchantment_3
}

is_evocation_1 = {
		has_ruler_flag = evocation_1
		NOT = { has_ruler_flag = evocation_2 }
		NOT = { has_ruler_flag = evocation_3 }
}

is_evocation_2 = {
		has_ruler_flag = evocation_2
		NOT = { has_ruler_flag = evocation_3 }
}

is_evocation_3 = {
		has_ruler_flag = evocation_3
}

is_illusion_1 = {
		has_ruler_flag = illusion_1
		NOT = { has_ruler_flag = illusion_2 }
		NOT = { has_ruler_flag = illusion_3 }
}

is_illusion_2 = {
		has_ruler_flag = illusion_2
		NOT = { has_ruler_flag = illusion_3 }
}

is_illusion_3 = {
		has_ruler_flag = illusion_3
}

is_necromancy_1 = {
		has_ruler_flag = necromancy_1
		NOT = { has_ruler_flag = necromancy_2 }
		NOT = { has_ruler_flag = necromancy_3 }
}

is_necromancy_2 = {
		has_ruler_flag = necromancy_2
		NOT = { has_ruler_flag = necromancy_3 }
}

is_necromancy_3 = {
		has_ruler_flag = necromancy_3
}

is_transmutation_1 = {
		has_ruler_flag = transmutation_1
		NOT = { has_ruler_flag = transmutation_2 }
		NOT = { has_ruler_flag = transmutation_3 }
}

is_transmutation_2 = {
		has_ruler_flag = transmutation_2
		NOT = { has_ruler_flag = transmutation_3 }
}

is_transmutation_3 = {
		has_ruler_flag = transmutation_3
}

precursor_relics_can_spawn = {
	OR = {
		AND = {
			OR = {
				is_year = 1560
				has_global_flag = castellos_is_dead
			}
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 5
					}
				}
			}
		}
		
		AND = {
			is_year = 1580
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 10
					}
				}
			}
		}
		
		AND = {
			is_year = 1600
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 15
					}
				}
			}
		}
		
		AND = {
			is_year = 1620
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 20
					}
				}
			}
		}
		
		AND = {
			is_year = 1650
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 30
					}
				}
			}
		}
	}
	NOT = {
		trade_goods = precursor_relics
		trade_goods = damestear
		trade_goods = gold
		trade_goods = gems
		has_province_flag = relics_no_spawn
		has_global_flag = max_precursor_relics
	}
	is_city = yes
}

damestear_can_spawn = {
	OR = {
		AND = {
			is_year = 1450
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 3
					}
				}
			}
		}
		
		AND = {
			is_year = 1520
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 12
					}
				}
			}
		}
		
		AND = {
			is_year = 1590
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 15
					}
				}
			}
		}
		AND = {
			is_year = 1650
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 20
					}
				}
			}
		}
	}
	any_owned_province = {
		is_city = yes
		NOT = {
			trade_goods = precursor_relics
			trade_goods = damestear
			trade_goods = gold
			trade_goods = gems
			has_province_flag = damestear_no_spawn
			has_province_flag = has_damestear
			has_terrain = dwarven_hold
			has_terrain = dwarven_hold_surface
		}
	}
}

relics_great_ruin = {
	NOT = {
		REB = {
			check_variable = {
				which = num_great_relic
				value = 5
			}
		}
	}
}

ruler_has_max_personalities = {
	calc_true_if = {
		ROOT = {
				ruler_has_personality = just_personality
				ruler_has_personality = righteous_personality
				ruler_has_personality = tolerant_personality
				ruler_has_personality = kind_hearted_personality
				ruler_has_personality = free_thinker_personality
				ruler_has_personality = well_connected_personality
				ruler_has_personality = calm_personality
				ruler_has_personality = careful_personality
				ruler_has_personality = secretive_personality
				ruler_has_personality = intricate_web_weaver_personality
				ruler_has_personality = fertile_personality
				ruler_has_personality = well_advised_personality
				ruler_has_personality = benevolent_personality
				ruler_has_personality = zealot_personality 
				ruler_has_personality = pious_personality
				ruler_has_personality = lawgiver_personality
				ruler_has_personality = midas_touched_personality
				ruler_has_personality = incorruptible_personality
				ruler_has_personality = architectural_visionary_personality
				ruler_has_personality = scholar_personality
				ruler_has_personality = entrepreneur_personality
				ruler_has_personality = industrious_personality
				ruler_has_personality = expansionist_personality
				ruler_has_personality = charismatic_negotiator_personality
				ruler_has_personality = conqueror_personality
				ruler_has_personality = silver_tongue_personality
				ruler_has_personality = tactical_genius_personality
				ruler_has_personality = bold_fighter_personality
				ruler_has_personality = strict_personality
				ruler_has_personality = inspiring_leader_personality
				ruler_has_personality = martial_educator_personality
				ruler_has_personality = navigator_personality
				ruler_has_personality = fierce_negotiator_personality
				ruler_has_personality = babbling_buffoon_personality
				ruler_has_personality = embezzler_personality
				ruler_has_personality = infertile_personality
				ruler_has_personality = drunkard_personality
				ruler_has_personality = sinner_personality
				ruler_has_personality = greedy_personality
				ruler_has_personality = cruel_personality
				ruler_has_personality = craven_personality
				ruler_has_personality = naive_personality
				ruler_has_personality = loose_lips_personality
				ruler_has_personality = obsessive_perfectionist_personality
				ruler_has_personality = malevolent_personality
				ruler_has_personality = immortal_personality   #We count it
				ruler_has_personality = mage_personality
		}
	
		amount = 3
	}
}

has_subterranean_race = {
	custom_trigger_tooltip = {
		tooltip = province_has_subterranean_race_tooltip
		OR = {
			has_province_modifier = dwarven_majority_coexisting
			has_province_modifier = dwarven_majority_integrated
			has_province_modifier = kobold_majority_coexisting
			has_province_modifier = kobold_majority_integrated
			has_province_modifier = goblin_majority_coexisting
			has_province_modifier = goblin_majority_integrated
		}
	}
}

has_ruinborn_culture = {	#fyi culture_group_is_ruinborn exists and is more updated
	OR = {
		culture_group = kheionai_ruinborn_elf
		culture_group = eltibhari_ruinborn_elf
		culture_group = devandi_ruinborn_elf
		culture_group = north_ruinborn_elf
		culture_group = eordellon_ruinborn_elf
		culture_group = south_ruinborn_elf
		culture_group = harafe_ruinborn_elf
		culture_group = effelai_ruinborn_elf
		culture_group = songwracked_elf
		culture_group = degenerated_elf
		culture_group = amadian_ruinborn_elf
		culture_group = ynnic_ruinborn_elf
		culture_group = veykodan_ruinborn_elf
	}
	NOT = { culture = duskers }
}

has_fey_loved_culture = {
	custom_trigger_tooltip = {
		tooltip = has_fey_loved_culture_tooltip
		OR = {
			culture = wood_elf
			culture = emerald_orc
			culture_group = eordellon_ruinborn_elf
		}
	}
}

can_no_longer_find_wonders = {
	OR = {
		AND = {
			has_country_modifier = pf_glowing_city
			north_america = { type = all has_discovered = ROOT }
			south_america = { type = all has_discovered = ROOT }
		}
		AND = {
			has_country_modifier = pf_dragon_house
			amadia_region = { type = all has_discovered = ROOT }
		}
		AND = {
			has_country_modifier = pf_elfenbride_manse
			rzenta_region = { type = all has_discovered = ROOT }
			forest_of_the_cursed_ones_region = { type = all has_discovered = ROOT }
			dalaire_region = { type = all has_discovered = ROOT }
		}
		AND = {
			has_country_modifier = pf_god_fragment_ruin
			colonial_noruin = { type = all has_discovered = ROOT }
		}
		AND = {
			has_country_modifier = pf_vault_ruin
			colonial_ynn = { type = all has_discovered = ROOT }
		}
		AND = {
			has_country_modifier = pf_portal_nexus_ruin
			north_america = { type = all has_discovered = ROOT }
			south_america = { type = all has_discovered = ROOT }
		}
		AND = {
			has_country_modifier = pf_floating_city_crash_site
			west_effelai_region = { type = all has_discovered = ROOT }
			east_effelai_region = { type = all has_discovered = ROOT }
			south_effelai_region = { type = all has_discovered = ROOT }
			leechdens_region = { type = all has_discovered = ROOT }
		}
	}
}
