government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = royal_harimari
add_accepted_culture = dhukharuved
religion = high_philosophy
technology_group = tech_harimari
religious_school = golden_palace_school
capital = 4417

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Kirtir"
		dynasty = "of the Imperious Stripe"
		birth_date = 1407.3.13
		adm = 5
		dip = 2
		mil = 2
		culture = royal_harimari
	}
}