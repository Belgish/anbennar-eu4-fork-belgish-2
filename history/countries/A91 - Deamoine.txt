government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = esmari
religion = regent_court
technology_group = tech_cannorian
capital = 333 # Deamoine
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1422.1.2 = { set_country_flag = is_a_county }

1440.1.12 = {
	monarch = {
		name = "Vincen II"
		dynasty = "s�l Deamoine"
		birth_date = 1421.11.1
		adm = 1
		dip = 5
		mil = 0
	}
}