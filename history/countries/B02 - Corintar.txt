government = theocracy
add_government_reform = adventurer_reform
government_rank = 1
primary_culture = corintari
religion = regent_court
technology_group = tech_cannorian
capital = 827
historical_rival = B01 #Greentide

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1400.1.2 = { set_country_flag = knightly_order_adventurer }

1444.11.11 = {
	monarch = {
		name = "Lothane Bluetusk"
		culture = half_orc
		birth_date = 1425.3.7
		adm = 3
		dip = 2
		mil = 6
	}
	add_ruler_personality = inspiring_leader_personality
}
